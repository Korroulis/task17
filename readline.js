const readline = require('readline');

const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

const message = `
Choose an option:
1. Read my package.json
2. Display OS info
3. Start HTTP server
-----------------
Type a number: `

rl.question(message, (answer) => {

    if (answer == 1) {
        console.log('You chose option ' + answer);
        console.log('Reading package.json file');
        const fs = require('fs');
        const obj = JSON.parse(fs.readFileSync(__dirname + '/package.json', 'utf8'));
        console.log(obj);

    } else if (answer == 2) {
        console.log('You chose option ' + answer);
        console.log('Getting OS info...');
        const os = require('os');
        const os_info = {
            "SYSTEM MEMORY": `${Math.round(os.totalmem() / 1024 / 1024 / 1024)} GB`,
            "FREE MEMORY": `${Math.round(os.freemem() / 1024 / 1024 / 1024)} GB`,
            "CPU CORES": `${os.cpus().length}`,
            "ARCH": `${os.arch()}`,
            "PLATFORM": `${os.platform()}`,
            "RELEASE": `${os.release()}`,
            "USER": `${os.userInfo().username}`
        };
        console.log(os_info);

    } else if (answer == 3) {
        console.log('You chose option ' + answer);
        console.log('Starting HTTP server...');

        const http = require('http');

        const hostname = 'localhost';
        const port = 5500;

        const server = http.createServer((req, res) => {
            res.statusCode = 200;
            res.setHeader('Content-Type', 'text/plain');
            res.write("Hello world");
            res.end("Hello world");


        });

        server.listen(port, () => {
            console.log(`Listening on port:${port}....`);

        });

    } else {
        console.log('Invalid option');

    }
    rl.close();
});